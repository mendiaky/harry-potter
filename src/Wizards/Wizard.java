package src.Wizards;

import src.Factory.Enums.Blood;
import src.Factory.Enums.Faculty;
import src.Factory.Enums.Race;
import src.objects.Wand;

public class Wizard {
    private final String name;
    private int age;
    private Blood blood;
    private Wand wand = null;
    private Faculty faculty;
    private String nature;
    private Race race;

    public Wizard(String name, Blood blood, String nature) {
        this.name = name;
        this.age = age;
        this.blood = blood;
        this.nature = nature;
    }
    public Wizard(String name, Race race){
        this.name = name;
        this.race = race;
    }

    public void setBlood(Blood blood) {
        this.blood = blood;
    }

    public Wand getWand() {
        return wand;
    }

    public void setWand(Wand wand) {
        this.wand = wand;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }
    public void setAge(int age){
        this.age = age;
    }

    public String getName(){
        return name;
    }
    public int calculatePowerWiz(){
        int power = 0;
        switch (nature){
            case "Brave": power += 25 + age/2;break;
            case "Smart": power += 40 + age/2;break;
            case "Calm": power += 30 + age/2;break;
            case "Cowardly": power += 24 + age/2;break;
            case "Selfish": power += 35 +age/2;break;
        }
        return power;
    }
    public int calculatePower(){
       return this.wand.calculatePower();
    }

    @Override
    public String toString(){
        String s = " ";
        if (this.wand!=null){
            s += wand.toString();
        }
        return "Name: " + name +", age: " + age + ". Blood: " + blood + ". Charac:" + nature + s;
    }

}
