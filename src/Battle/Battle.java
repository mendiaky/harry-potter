package src.Battle;

import src.Wizards.Wizard;

public class Battle {
    public int rounds = 3;

    public void startBattle(Wizard wizard1, Wizard wizard2){
        int wiz1Score = 0;
        int wiz2Score = 0;
        int i1 = wizard1.calculatePower() - 30;
        int i2 = wizard1.calculatePower() - 30;
        for (int i = 1; i <= 3; i++){
            int powerRoundWiz1 = (int)(Math.random() * (wizard1.calculatePower() - i1) + i1);
            int powerRoundWiz2 = (int)(Math.random() * (wizard2.calculatePower() - i2) + i2);
            if (powerRoundWiz1 > powerRoundWiz2){
                wiz1Score += 1;
                System.out.println("\n" +i + " round won wizard " + wizard1.getName());
            }else {
                wiz2Score += 1;
            System.out.println("\n" +i + " round won wizard " + wizard2.getName());
            }
        }
        if (wiz1Score>wiz2Score){
            System.out.println("==========================================");
            System.out.println("\n"+wizard1.getName() + " won the fight!");
            System.out.println("==========================================");
        }else {
            System.out.println("==========================================");
            System.out.println("\n"+wizard2.getName() + " won the fight!");
            System.out.println("==========================================");
        }

    }
}
