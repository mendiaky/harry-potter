package src.Factory;

import src.Factory.Enums.HeartStone;
import src.Factory.Enums.Material;
import src.Wizards.Wizard;
import src.objects.Wand;

public class WandFactory extends Wand{

    public Wand createWand(Wizard wizard){
        Wand wand = new Wand();
        wand.chooseByCharacteristics(wizard);
        wizard.setWand(wand);
        wand.setRandomLength();
        wand.chooseRandomHeart();
        wand.setCurrentOwner(wizard);
        return wand;
    }




}
