package src.Factory;

import src.Factory.Enums.Blood;
import src.Factory.Enums.Race;
import src.Wizards.Wizard;

public class WizardFactory {

    public Wizard createWizard(String name,Race race){
        Wizard wizard = new Wizard(name,race);
        chooseCharacteristics(wizard);randomAge(wizard);
        randomBlood(wizard);
        return wizard;
    }

    public void chooseCharacteristics(Wizard wizard){
        int randomInt = (int)(Math.random() * (6-2)+2);
        switch (randomInt){
            case 2: wizard.setNature("Brave");break;
            case 3: wizard.setNature("Smart");break;
            case 4: wizard.setNature("Calm");break;
            case 5: wizard.setNature("Selfish");break;
            case 6: wizard.setNature("Cowardly");break;
        }
    }
    public void randomAge(Wizard wizard){
        int randomAge = (int)(Math.random() * (25-17) + 17);
        wizard.setAge(randomAge);
    }
    public void randomBlood(Wizard wizard){
        int randomBlood = (int)(Math.random() * (4-2)+2);
        switch (randomBlood){
            case 2:wizard.setBlood(Blood.highBlood);break;
            case 3:wizard.setBlood(Blood.halfBreed);break;
            case 4:wizard.setBlood(Blood.Goblin);break;
        }
    }

}
