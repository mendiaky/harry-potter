package src.objects;

import src.Factory.Enums.HeartStone;
import src.Factory.Enums.Material;
import src.Wizards.Wizard;

import java.util.HashMap;
import java.util.Map;

public class Wand {
    private Wizard currentOwner;
    private Material materialWood;
    private double length;
    private HeartStone heartStone;

    public Wizard getCurrentOwner() {
        return currentOwner;
    }

    public void setCurrentOwner(Wizard currentOwner) {
        this.currentOwner = currentOwner;
    }

    public int calculatePower(){
        Map<HeartStone,Integer> heartMinus = new HashMap<>();
        heartMinus.put(HeartStone.Dragon, 2);
        heartMinus.put(HeartStone.Fenix, 5);
        heartMinus.put(HeartStone.Troll, 9);
        heartMinus.put(HeartStone.Unicorn, 4);
        heartMinus.put(HeartStone.Festral, 6);

        int power = 0;
        switch (materialWood){
            case Ash -> power += 28 ; //Calm
            case Apple -> power += 25; //Brave
            case Aspen -> power += 20; // Cowardly
            case Beech -> power += 25;// Selfish
            case Acacia -> power += 36;// Smart
            case BlackWalnut -> power += 36;// Smart
            case Cypress -> power += 25;// Brave
            case Ulmus -> power += 25;// Selfish
            case Cedar -> power += 25;// Brave
            case Cherry -> power += 20;// Cowardly
            case Chestnut -> power += 25;// Selfish
        }
        power -= heartMinus.get(this.heartStone);
        power += this.currentOwner.calculatePowerWiz();
        return power;
    }

    public Material getMaterialWood() {
        return materialWood;
    }

    public void setMaterialWood(Material materialWood) {
        this.materialWood = materialWood;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public HeartStone getHeartStone() {
        return heartStone;
    }

    public void setHeartStone(HeartStone heartStone) {
        this.heartStone = heartStone;
    }

    public void chooseByCharacteristics(Wizard wizard){

        switch (wizard.getNature()){
            case "Brave": int randomInt = (int)(Math.random() * (4-2)+2);
                switch (randomInt){
                    case 2: this.setMaterialWood(Material.Apple);break;
                    case 3: this.setMaterialWood(Material.Cedar);break;
                    case 4: this.setMaterialWood(Material.Cypress);
                }break;
            case "Selfish": int randomInt2 = (int)(Math.random() * (3-2)+2);
                switch (randomInt2){
                    case 2: this.setMaterialWood(Material.Beech);break;
                    case 3: this.setMaterialWood(Material.Chestnut);break;
                }break;
            case "Smart": int randomInt3 = (int)(Math.random() * (3-2)+2);
                switch (randomInt3){
                    case 2: this.setMaterialWood(Material.Acacia);break;
                    case 3: this.setMaterialWood(Material.BlackWalnut);break;
                }break;
            case "Calm": this.setMaterialWood(Material.Ash);break;
            case "Cowardly": int randomInt4 = (int)(Math.random() * (3-2)+2);
                switch (randomInt4){
                    case 2: this.setMaterialWood(Material.Aspen);break;
                    case 3: this.setMaterialWood(Material.Cherry);break;
                }break;
        }
    }
    public void setRandomLength() {
        double length = (Math.random()*(38.4-23.5)+23.5);
        this.setLength(length);
    }
    public void chooseRandomHeart() {
        int randomInt = (int)(Math.random() * (6-2)+2);
        switch (randomInt){
            case 2: this.setHeartStone(HeartStone.Troll);break;
            case 3: this.setHeartStone(HeartStone.Fenix);break;
            case 4: this.setHeartStone(HeartStone.Festral);break;
            case 5: this.setHeartStone(HeartStone.Unicorn);break;
            case 6: this.setHeartStone(HeartStone.Dragon);break;
        }
    }
    @Override
    public String toString(){

        return "[Material: " + materialWood + ", length: " + String.format("%.2f",length) +"cm" + " Heartstone: " + heartStone + "]";
    }
}
