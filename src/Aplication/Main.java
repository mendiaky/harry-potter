package src.Aplication;

import src.Battle.Battle;
import src.Factory.Enums.Race;
import src.Factory.WandFactory;
import src.Factory.WizardFactory;
import src.Wizards.Wizard;

public class Main {
    public static void main(String[] args) {
        WizardFactory wizardFactory = new WizardFactory();
        Wizard wiz1 = wizardFactory.createWizard("Albus Dambldor", Race.Human);
        Wizard wiz2 = wizardFactory.createWizard("Germiona Greindger", Race.Human);
        Wizard wiz3 = wizardFactory.createWizard("Harry Potter", Race.Human);

        System.out.println("==============================================");
        WandFactory wandFactory = new WandFactory();
        wandFactory.createWand(wiz1);
        wandFactory.createWand(wiz2);
        wandFactory.createWand(wiz3);

        System.out.println(wiz1);
        System.out.println(wiz1.calculatePower());
        System.out.println(wiz2);
        System.out.println(wiz2.calculatePower());
        System.out.println(wiz3);
        System.out.println(wiz3.calculatePower());

        Battle battle = new Battle();

        battle.startBattle(wiz1,wiz2);
        battle.startBattle(wiz1,wiz3);

    }
}
